exports.appIndexRender = function(path,app,express,exphbs,request,response) {
    app.engine('.hbs', exphbs({  
        defaultLayout: 'ancestry',
        extname: '.hbs',
        layoutsDir: path.join(__dirname, '/views/layouts')
    }))
    app.set('view engine', '.hbs')  
    app.set('views', path.join(__dirname, '/views'))  
    response.render('home.hbs')
    var date = new Date()
//    var current_hour = date.getHours();
    console.log('rendered home at ' + date)
}