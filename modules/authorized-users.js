module.exports = function(request) {
    var user = {
        id: request.query["user"],
        valid: false
    }
    if (user.id !== undefined) {
        var authorized = [
                "svw",
                "rpainter",
                "jlangston"
            ]
        if (authorized.indexOf(user.id) > -1) {
            user.valid = true;
        }
    }
    return user;
}