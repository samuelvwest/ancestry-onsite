module.exports = {
    create: function() {
        var assets = require(fp('data/assets.json'))
        var targets = require(fp('data/targets.json'))
        var targetTypes = require(fp('data/target-types.json'))
        var categories = require(fp('data/categories.json'))
        var places = require(fp('data/places.json'))
        var accessTools = require(fp('data/access-tools.json'))
        var geos = require(fp('data/geos.json'))
        var campaigns = require(fp('data/campaigns.json'))
        var masterJson = { onsite: {
            assets: assets.assets,
            targets: targets.targets,
            targetTypes: targetTypes.targetTypes,
            categories: categories.categories,
            places: places.places,
            accessTools: accessTools.accessTools,
            campaigns: campaigns.campaigns,
            geos: geos.geos
        }}
        return masterJson;
    },
    replace: function(masterJson) {
        var jsonfile = require('jsonfile')
        jsonfile.writeFile(fp('lib/json/master.json'), masterJson)
    },
    assetDetails: function(assetId, mJson) {
        mJson.tADetails = mJson.onsite.assets[assetId]
        for (var place in mJson.onsite.places) { mJson.onsite.places[place]['selected'] = mJson.tADetails.place === place ? true : false; };
        for (var geo in mJson.onsite.geos) { mJson.onsite.geos[geo]['selected'] = mJson.tADetails.geo === geo ? true : false; };
        for (var category in mJson.onsite.categories) { mJson.onsite.categories[category]['selected'] = mJson.tADetails.category === category ? true : false; };
        for (var tool in mJson.onsite.accessTools) { mJson.onsite.accessTools[tool]['selected'] = mJson.tADetails.tool === tool ? true : false; };
        for (var camp in mJson.onsite.campaigns) { mJson.onsite.campaigns[camp]['checked'] = mJson.tADetails.campaigns.indexOf(camp) > -1 ? true : false; };
        for (var target in mJson.onsite.targets) { mJson.onsite.targets[target]['checked'] = mJson.tADetails.targets.indexOf(target) > 0 ? true : false; };
        var tagsLength = mJson.tADetails.tags.length;
        var tagsCount = 1;
        for (var tag in mJson.tADetails.tags) { 
            mJson.tADetails.tags[tag] = { 
                "name": mJson.tADetails.tags[tag],
                "comma": tagsCount !== tagsLength ? true : false
            };
            tagsCount++;
        };
        return mJson;
    }
}