module.exports = function(app,exphbs) {
    app.engine('.hbs', exphbs({
        helpers: {
            uppercase: function (str) { 
                return str.toUpperCase(); 
            },
            targetNames: function (assetTargets,onsiteTargets) { 
                var tNames = '',
                    connector = ' : ';
                for (i = 1; i < assetTargets.length; i++) {
                    if ((i + 1) === assetTargets.length) {
                        connector = '';
                    }
                    tNames += onsiteTargets[assetTargets[i]].name + connector;
                };
                return tNames;
            },
            anchorLink: function (link,text,appendage) {
                var a = typeof appendage !== 'undefined' ? appendage : '',
                    l = link + a,
                    t = typeof text !== 'undefined' ? text !== "" ? text : l : l;
                return '<a href="' + l + '">' + t + '</a>';
            },
            nameFromId: function (id,obj) {
                return obj[id].name;
            },
            targetsByType: function (typeId, targetsObj) {
                var targetsHtml = '';
                for (var tar in targetsObj) {
                    var t = targetsObj[tar],
                        c = t.checked ? ' checked' : '';
                    if (t.type === typeId) {
                        targetsHtml += '<input type="checkbox" name="targets" value="' + tar + '" + ' + c + '> ' + t.name + '<br />';
                    };
                };
                return targetsHtml;
            },
            expressTargetElements: function (targArr,targetsObj){
                function wrapContentElem(tag,primaryClass,classes,content) {
                    var wrapElem = '<' + tag + ' class="' + primaryClass;
                    for (var c in classes) {
                        wrapElem += ' ' + classes[c];
                    };
                    wrapElem += '">' + content + '</' + tag + '>';
                    return wrapElem;
                };
                function propogateTargets(tA,tO,tN) {
                    var tObj = {
                        "name": tN ? tN : '',
                        "connector": tA.length > 2 ? ' ' + tA[0] : '',
                        "targets": tA.slice(1)
                    };
    //                console.log(tObj.targets);
                    for (var cT in tObj.targets) {
                        var c = tObj.targets[cT];
                        if (tO[c]) {
                            var tarDet = tO[c];
    //                        console.log('found target in object: ', tarDet);
                            tObj.targets[cT] = propogateTargets(tarDet.target,tO,tarDet.name);
                        } else if (typeof c === "object") {
    //                    } else {
    //                        console.log(typeof c, c);
    //                        console.log('empty target object: ', c);
                            tObj.targets[cT] = propogateTargets(c,tO);
                        };
                    };
                    return tObj;
                };
                function createTargetElements(tWObj,first) {
                    var elem = '',
                        twoObjTest = 0;
                    if (tWObj.targets === 'undefined') {
    //                    console.log(tWObj);
                        elem += wrapContentElem('div','targetDetail',[],tWObj);
                        twoObjTest++;
                    } else {
                        for (var target in tWObj.targets) {
                            var newTWObj = tWObj.targets[target];
    //                        console.log(tWObj,newTWObj,typeof newTWObj);
                            if (typeof newTWObj === 'string') {
                                elem += wrapContentElem('div','targetDetail',[],newTWObj);
                            } else {
    //                            console.log(newTWObj);
                                elem += createTargetElements(newTWObj);
    //                            console.log(elem);
                            };
                            twoObjTest++;
    //                        createTargetElements(newTWObj);
                        };
                    };
                    var classArr = [tWObj.connector];
                    if (tWObj.name) {
                        elem = wrapContentElem('div','targetName',[],tWObj.name) + elem;
                        classArr.push('targetNameProvided');
                    };
                    if (twoObjTest < 2) {
                        classArr.push('targetSingle');
                    };
                    if (first) {
                        classArr.push('first');
                    };
                    elem = wrapContentElem('div','targetElem',classArr, elem);
    //                console.log(tWObj,elem);
                    return elem;
                };
                var tWObj = propogateTargets(targArr,targetsObj);
                return createTargetElements(tWObj,true);
            }
        }, 
        defaultLayout: 'ancestry',
        extname: '.hbs',
        layoutsDir: fp('views/layouts')
    }))
    //app.engine('.hbs', handlebars.engine);
    app.set('view engine', '.hbs')  
    app.set('views', fp('views/pages'))  
}