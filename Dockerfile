FROM node:7.10
MAINTAINER Onsite Optimization Production "swest@ancestry.com"

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app
RUN npm install

# Bundle app source
COPY . /usr/src/app

EXPOSE 5577:5577
CMD ["npm", "start"]