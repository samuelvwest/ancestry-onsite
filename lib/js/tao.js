// Accordions
$('.accordionBtn').click(function(){
    var aBtn = $(this),
        aTag = aBtn.attr('accordion-tag'),
        aContent = $('.accordionContent[accordion-tag="' + aTag + '"]')
        aVisibleContent = $('.accordionContent[accordion-tag="' + aTag + '"]:visible');
    if (aBtn.hasClass('accordionOut') || aVisibleContent.length > 0) {
        aBtn.removeClass('accordionOut');
        aContent.slideUp(250);
    } else {
        aBtn.addClass('accordionOut');
        aContent.slideDown(250);
    }
});

// Targeting Layout Change
$('.targetingLayoutChange').click(function(){
    $(this).toggleClass('details');
    $(this).parent().children('h6').toggleClass('details');
    $(this).parent().children('.targetElem').toggleClass('details');
});
$('.targetElem.first').click(function(){
    $(this).toggleClass('details');
    $(this).parent().children('h6').toggleClass('details');
    $(this).parent().children('.targetingLayoutChange').toggleClass('details');
});