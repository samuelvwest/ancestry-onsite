// Setup Universial paths
global.base_dir = __dirname;
global.fp = function(filePathFromBase) {
    return base_dir + '/' + filePathFromBase;
}

// Define Constants
const fs = require('fs')
const path = require('path')
const express = require('express')  
const exphbs = require('express-handlebars')
//const hbsHelpers = require('handlebars-helpers')
const bodyParser = require('body-parser')
const app = express()  
const port = 5577
const dashboard = path.join(__dirname, '/dashboard')

// Setup Handlebars render engines
require('./modules/handlebars-engine.js')(app,exphbs)


// Make sure all lib files can be references as static content
app.use('/lib', express.static(path.join(__dirname, 'lib')));

// HOME (goes to dashboard)
app.get(['','/'], (request ,response) => {
    response.redirect("/dashboard")
    var date = new Date()
    console.log('redirected to Dashboard at ' + date)
})

// Setup accessible user pages
// DASHBOARD
app.get('/dashboard*', (request, response) => {  
    delete require.cache[require.resolve(fp('data/assets.json'))]
    var master = require('./modules/master-json.js')
    var mJson = master.create()
    master.replace(mJson)
    mJson.user = require('./modules/authorized-users.js')(request);
    mJson.title = "Dashboard";
    mJson.query = request.query;
    response.render(fp('views/pages/dashboard.hbs'), mJson)
    var date = new Date()
    console.log('rendered Dashboard at ' + date)
})
// CREATE ASSET PAGE
app.get('/assets/create', (request, response) => {  
    delete require.cache[require.resolve(fp('data/targets.json'))]
    delete require.cache[require.resolve(fp('data/assets.json'))]
    var master = require('./modules/master-json.js')
    var mJson = master.create()
    master.replace(mJson)
    mJson.title = "Create Asset";
    response.render(fp('views/pages/assets/create-asset.hbs'), mJson)
    var date = new Date()
    console.log('rendered ' + mJson.title + ' at ' + date)
})
// UPDATE ASSET PAGE
app.get('/assets/update/*', (request, response) => {  
    delete require.cache[require.resolve(fp('data/targets.json'))]
    delete require.cache[require.resolve(fp('data/assets.json'))]
    var master = require('./modules/master-json.js')
    var mJson = master.create()
    master.replace(mJson)
    var assetId = request.url.substring(15)
    var passJson = master.assetDetails(assetId,mJson)
    mJson.title = "Update " + mJson.tADetails.name;
    response.render(fp('views/pages/assets/update-asset.hbs'), passJson)
    var date = new Date()
    console.log('rendered ' + assetId + ' at ' + date)
})
// LIST PERFORMANCE PREP ASSETS (TEMPORARY)
app.get('/assets/list/performance-prep', (request, response) => {  
    var master = require('./modules/master-json.js')
    var mJson = master.create()
    for (var asset in mJson.onsite.assets) {
        var tA = mJson.onsite.assets[asset],
            cTest = tA.category === 'lohp',
            iTest = tA.displayUrl.indexOf('ireland') === -1,
            fTest = cTest && iTest;
        tA.isperformance = fTest;
    };
    mJson.title = "LOHP Performance Prep";
    response.render(fp('views/pages/performance-prep.hbs'), mJson)
    var date = new Date()
    console.log('rendered Performance Prep at ' + date)
})

// ENABLE DIRECT USE OF DATABASE IN FRONT-END SCRIPT
app.get('/data/*', (request, response) => {  
    var dataFile = require('./' + request.url)
    response.json(dataFile)
})


// Setup bodyparser for actual use
app.use(bodyParser.json());

app.post('/api/assets/create', (request, response) => { 
    var crudAssets = require('./modules/assets/crud-assets.js')
    crudAssets.create(request.body)
    response.send(request.body); 
})

app.post('/api/assets/update/*', (request, response) => { 
    var crudAssets = require('./modules/assets/crud-assets.js')
    var oAId = request.url.substring(19)
    var responseToSend = crudAssets.update(oAId,request.body)
    console.log('Request Type : ' + responseToSend);
    response.send(JSON.stringify(responseToSend))
})

app.post('/api/assets/delete/*', (request, response) => { 
    var crudAssets = require('./modules/assets/crud-assets.js')
    var assetId = request.url.substring(19)
    crudAssets.delete(assetId)
    console.log('Deleted : ' + assetId);
    response.send(JSON.stringify('Deleted : ' + assetId))
})

app.use((err, request, response, next) => {  
    console.log(err)
    response.status(500).send('Something broke!')
})

// Setup server to listen on a port
app.listen(port, '0.0.0.0', (err) => {  
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log('server is listening on ' + port)
})